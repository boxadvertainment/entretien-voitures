var elixir   = require('laravel-elixir');

require('./elixir-extensions');

elixir(function(mix) {
    mix
        .sass('main.scss', 'public/css/main.css')
        .babel([
          //'facebookUtils.js',
          'main.js'
        ], 'public/js/main.js')
        .scripts([
            // bower:js
            'vendor/bower_components/jquery/dist/jquery.js',
            'vendor/bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
            'vendor/bower_components/sweetalert/dist/sweetalert.min.js',
            // endbower
        ], 'public/js/vendor.js', 'vendor/bower_components')
        .styles([
            // bower:css
            'vendor/bower_components/sweetalert/dist/sweetalert.css',
            // endbower
        ], 'public/css/vendor.css', 'vendor/bower_components')
        .version(['css/main.css', 'js/main.js'])
        .copy('public/fonts', 'public/build/fonts')
        .images()
        //.copy('vendor/bower_components/font-awesome/fonts', 'public/fonts')
        .wiredep({
            //exclude: ['jquery']
        })
        .browserSync({
            proxy: process.env.APP_URL
        });
});
