<?php

namespace App\Console;

use Carbon\Carbon;
use App\Car;
use Mail;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $now = new Carbon('+3 days');
            $cars = Car::all();

            foreach ($cars as $car)
            {
                $dvt = new Carbon($car->date_visite_tecknique);
                $da = new Carbon($car->date_assurance);
                $dv = new Carbon($car->date_vignette);

                if ( $now > $dvt || $now > $da || $now > $dv) {

                    Mail::send(['raw' => 'Veuillez vérifier les dates des entretiens de la voiture "' . $car->nom . '" Matricule "'. $car->matricule .'".'], [], function ($message) {
                        $message->from(\Config::get('app.email'), \Config::get('app.name'));
                        $message->to('marwen@box.agency')->subject('Rappel d\'entretien de voiture');
                    });
                }
            }
        })->daily();
    }
}
