<?php

Route::get('/', 'AppController@home');
Route::post('/update/{id}', 'AppController@update');
Route::get('/sendMail', 'AppController@sendMail');
Route::get('/modal/{id}','NoteController@index');
Route::post('/addNote', 'NoteController@store');