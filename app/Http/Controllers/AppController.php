<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;

use App\Car;
use Carbon\Carbon;

class AppController extends Controller
{
    public function home()
    {
        $cars=Car::all();
        return view('index',compact('cars'));
    }

    public function update($id, Request $request){
        $car = Car::findOrFail($id);
        $car->date_visite_tecknique = $request->date_visite_tecknique;
        $car->date_vignette = $request->date_vignette;
        $car->date_assurance = $request->date_assurance;
        $car->vidange = $request->vidange;
        $car->prevision = $request->prevision;
        $car->save();
        return redirect('/');
    }
}