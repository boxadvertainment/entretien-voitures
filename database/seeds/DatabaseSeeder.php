<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('cars')->insert([
            ['id' => 1, 'nom' => 'Citroen Nemo', 'matricule' => '3837 TU 166', 'mise_circulation' => new DateTime('07/06/2013'), 'num_chassis' => 'VF7AA8HSCD8381278', 'puissance' => 6 ],
            ['id' => 2, 'nom' => 'Citroen Nemo', 'matricule' => '3840 TU 166', 'mise_circulation' => new DateTime('07/06/2013'), 'num_chassis' => 'VF7AA8HSCD8380879', 'puissance' => 6 ],
            ['id' => 3, 'nom' => 'Renault Symbole', 'matricule' => '2560 TU 162' , 'mise_circulation' => new DateTime('07/06/2013'), 'num_chassis' => 'VF7AA8HSCD8381278', 'puissance' => 5 ]
        ]);

        Model::reguard();
    }
}
