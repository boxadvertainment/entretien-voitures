<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('matricule');
            $table->string('nom');
            $table->date('mise_circulation');
            $table->string('num_chassis');
            $table->smallInteger('puissance');
            $table->date('date_visite_tecknique');
            $table->date('date_vignette');
            $table->date('date_assurance');
            $table->string('vidange');
            $table->string('prevision');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cars');
    }
}