@extends('layout')
@section('content')
    <h1>Update car</h1>
    {!! Form::model($car,['method' => 'PATCH','route'=>['cars.update',$car->id]]) !!}
    <div class="form-group">
        {!! Form::label('nom', 'nom:') !!}
        {!! Form::text('nom',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('matricule', 'matricule:') !!}
        {!! Form::text('matricule',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('date_visite_tecknique', 'date_visite_tecknique:') !!}
        {!! Form::text('date_visite_tecknique',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('date_vignette', 'date_vignette:') !!}
        {!! Form::text('date_vignette',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('date_assurance', 'date_assurance:') !!}
        {!! Form::text('date_assurance',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('vidange', 'vidange:') !!}
        {!! Form::text('vidange',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
@stop