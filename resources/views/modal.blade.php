@extends('layout')

@section('content')
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <span class="modal-title">Ajouter Des Notes</span>
    </div>
        <form id="note-form" action="{{ action('NoteController@store') }}" method="post">
            <div class="modal-body clearfix">
                <div class="col-xs-10 col-xs-offset-1">
                    {!! csrf_field() !!}
                    <!--<p>Ajouter des note pour chaque voiture</p>-->
                    <div class="form-group name">
                        <label class="control-label">Note:</label>
                        <div class="inputs">
                            <textarea rows="4" class="form-control" name="note" id="note"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="carId" id="carId" value="<?php echo $id; ?>" >
                    <button type="submit" class="btn btn-primary">Add Note</button>
                </div>
            </div>
        </form>
        @if ($notes)
            <div class="modal-footer">
                <ul class="list-group">
                    @foreach ($notes as $note)
                        <li class="list-group-item text-center list-group-item-warning">{{ $note->note }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
@endsection