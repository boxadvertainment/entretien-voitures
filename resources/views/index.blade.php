@extends('layout')

@section('content')
    <div class="container-fluid">
        <div class="col-sm-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <h1 style="text-align:center;">Entretien Voitures</h1>
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped task-table">
                            <thead>
                                <tr>
                                    <th width="130px" class="table-text">Type de Voiture</th>
                                    <th width="100px" class="table-text">Matricule</th>
                                    <th width="80px" class="table-text">Date de Mise en Circulation</th>
                                    <th class="table-text">N° de Chassis</th>
                                    <th width="80px" class="table-text">Puissance Fiscale</th>
                                    <th class="table-text">Visite Technique</th>
                                    <th class="table-text">Vignette</th>
                                    <th class="table-text">Assurance</th>
                                    <th class="table-text">Vidange</th>
                                    <th class="table-text">Prevision</th>
                                    <th class="table-text">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($cars as $car)
                                <tr>
                                        <form action="{{ action('AppController@update', ['id' => $car->id]) }}" method="POST" class="form-horizontal">

                                        {!! csrf_field() !!}

                                        <td class="table-text">
                                            <div>{{ $car->nom }}</div>
                                        </td>

                                        <td>
                                            <div>{{ $car->matricule }}</div>
                                        </td>

                                        <td>
                                            <div>{{ $car->mise_circulation }}</div>
                                        </td>

                                        <td>
                                            <div>{{ $car->num_chassis }}</div>
                                        </td>

                                        <td>
                                            <div>{{ $car->puissance }}</div>
                                        </td>

                                        <td>
                                            <input type="date" name="date_visite_tecknique" id="date_visite_tecknique" value="{{ $car->date_visite_tecknique }}" class="form-control">
                                        </td>

                                        <td>
                                            <input type="date" name="date_vignette" id="date_vignette" value="{{ $car->date_vignette}}" class="form-control">
                                        </td>

                                        <td>
                                            <input type="date" name="date_assurance" id="date_assurance" value="{{ $car->date_assurance}}" class="form-control">
                                        </td>

                                        <td>
                                            <input type="text" name="vidange" id="vidange" value="{{ $car->vidange}}" class="form-control">
                                        </td>

                                        <td>
                                             <input type="text" name="prevision" id="prevision" value="{{ $car->prevision}}" class="form-control">
                                        </td>

                                        <td>
                                            <a  href="./modal/{{ $car->id }}" class="btn btn-default" data-car="{{ $car->id }}" data-target="#myModal" id="openmodal" data-toggle="modal">Notes</a>
                                            <input type="submit" value="Save" class="btn btn-primary" />
                                        </td>
                                    </form>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <iframe src="" frameborder="0" class="embed-responsive-item" id="iframe" width="100%" height="500"></iframe>
            </div>
        </div>
    </div>
@endsection